# Asciidoctor regex-sub Extension
:version: 0.0.1

`@djencks/asciidoctor-regex-sub` provides an Asciidoctor.js extension.

NOTE: for more complete, better formatted README, see https://gitlab.com/djencks/asciidoctor-regex-sub/-/blob/master/README.adoc.

## Installation

Available soon through npm as @djencks/asciidoctor-regex-sub.

Currently available through a line like this in `package.json`:


    "@djencks/asciidoctor-regex-sub": "https://experimental-repo.s3-us-west-1.amazonaws.com/djencks-asciidoctor-regex-sub-v0.0.1.tgz",


The project git repository is https://gitlab.com/djencks/asciidoctor-regex-sub

## Usage in asciidoctor.js

see https://gitlab.com/djencks/asciidoctor-regex-sub/-/blob/master/README.adoc

## Usage in Antora

see https://gitlab.com/djencks/asciidoctor-regex-sub/-/blob/master/README.adoc

[comment]: <> (## Antora Example project)

[comment]: <> (An example project showing some uses of this extension is under extensions/asciidoctor-regex-sub-extension in `https://gitlab.com/djencks/simple-examples`.)
