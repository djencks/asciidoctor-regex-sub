'use strict'

module.exports = {
  RESULT: 'a class="el" href="dir_8d0824bbeddf28068400ad1b089826bf.html"',
  RESULT_FRAGMENT: 'a class="el" href="dir_8d0824bbeddf28068400ad1b089826bf.html#fragment.adoc"',
  REGEX: '{ "(href=\\"[^.#]*)\\\\.adoc((#([^\\"]*))?\\")": "$1.html$2" }',
  BAD_REGEX: '{ "(href=\\"[^.#]*)\\\\.adoc((#([^\\"]*))?\\")": "$1.xhtml$2" }',
}
