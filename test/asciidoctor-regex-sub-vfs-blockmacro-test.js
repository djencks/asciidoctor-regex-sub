/* eslint-env mocha */
'use strict'

//Opal is apt to be needed for Antora tests, perhaps not otherwise
// IMPORTANT eagerly load Opal since we'll always be in this context; change String encoding from UTF-16LE to UTF-8
const { Opal } = require('asciidoctor-opal-runtime')
if ('encoding' in String.prototype && String(String.prototype.encoding) !== 'UTF-8') {
  String.prototype.encoding = Opal.const_get_local(Opal.const_get_qualified('::', 'Encoding'), 'UTF_8') // eslint-disable-line
}
const asciidoctor = require('@asciidoctor/core')()
const asciidoctorRegexSub = require('./../lib/asciidoctor-regex-sub')
const { RESULT, RESULT_FRAGMENT, REGEX, BAD_REGEX } = require('./constants')
const { expect } = require('chai')

const mockContentCatalog = require('./antora-mock-content-catalog')

describe('asciidoctor-regex-sub blockmacro vfs tests', () => {
  beforeEach(() => {
    asciidoctor.Extensions.unregisterAll()
  })

  function prepareVfss (seed) {
    if (!Array.isArray(seed)) seed = [seed]
    const vfss = [
      {
        vfsType: 'plain',
        config: {
          vfs: {
            read: (filespec) => {
              filespec = filespec.substring(filespec.lastIndexOf(':') + 1)
              filespec = filespec.substring(filespec.lastIndexOf('$') + 1)
              return seed.filter((f) => filespec === f.relative).reduce((accum, f) => {
                accum.contents = Buffer.from(f.contents)
                return accum
              }, {}).contents
            },

          },
        },
      },
      {
        vfsType: 'antora',
        config: {
          file: { src: {} },
          contentCatalog: mockContentCatalog(seed),
          config: { attributes: {} },
        },
      },
    ]
    return vfss
  }

  ;[
    // {
    //   type: 'global',
    //   f: (text, config) => {
    //     asciidoctorRegexSub.register(asciidoctor.Extensions, config)
    //     return asciidoctor.load(text)
    //   },
    // },
    {
      type: 'registry',
      f: (text, config) => {
        const registry = asciidoctorRegexSub.register(asciidoctor.Extensions.create(), config)
        return asciidoctor.load(text, { extension_registry: registry })
      },
    },
  ].forEach(({ type, f }) => {
    prepareVfss(
      [
        {
          version: '4.5',
          family: 'example',
          relative: 'example-href.adoc',
          contents: '<a class="el" href="dir_8d0824bbeddf28068400ad1b089826bf.adoc">',
        },
        {
          version: '4.5',
          family: 'example',
          relative: 'example-href-fragment.adoc',
          contents: `
<a class="el" href="dir_8d0824bbeddf28068400ad1b089826bf.adoc#fragment.adoc">
`,
        },
      ]
    ).forEach(({ vfsType, config }) => {
      [
        [
          '4.5@component-a:module-a:example$example-href.adoc',
          RESULT,
          'plain',
        ],
        [
          '4.5@component-a:module-a:example$example-href-fragment.adoc',
          RESULT_FRAGMENT,
          'fragment',
        ],
      ].forEach(([target, expected, linkType]) => {
        [
          ['====', 'example'],
          ['----', 'listing'],
          ['....', 'literal'],
          ['--', 'open'],
          ['++++', 'pass'],
          ['____', 'quote'],
          ['****', 'sidebar'],
        ].forEach(([delimiter, context]) => {
          it(`doc level regex, ${type}, ${vfsType}, ${linkType}, ${context}`, () => {
            const doc = f(`
:regex-sub: ${REGEX}

regexSub::${target}[${context}]
`, config)
            const html = doc.convert()
            expect(html).to.contain(expected)
          })

          it(`macro level regex, ${type}, ${vfsType}, ${linkType}, ${context}`, () => {
            const doc = f(`
regexSub::${target}[${context},${REGEX}]
`, config)
            const html = doc.convert()
            expect(html).to.contain(expected)
          })

          it(`macro level regex overrides doc level regex, ${type}, ${vfsType}, ${linkType}, ${context}`, () => {
            const doc = f(`
:regex-sub: ${BAD_REGEX}

regexSub::${target}[${context},${REGEX}]
`, config)
            const html = doc.convert()
            expect(html).to.contain(expected)
          })
        })
      })
    })
  })
})
