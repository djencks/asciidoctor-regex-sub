/* eslint-env mocha */
'use strict'

//Opal is apt to be needed for Antora tests, perhaps not otherwise
// IMPORTANT eagerly load Opal since we'll always be in this context; change String encoding from UTF-16LE to UTF-8
const { Opal } = require('asciidoctor-opal-runtime')
if ('encoding' in String.prototype && String(String.prototype.encoding) !== 'UTF-8') {
  String.prototype.encoding = Opal.const_get_local(Opal.const_get_qualified('::', 'Encoding'), 'UTF_8') // eslint-disable-line
}
const asciidoctor = require('@asciidoctor/core')()
const asciidoctorregexSub = require('./../lib/asciidoctor-regex-sub')
const parseRegexes = asciidoctorregexSub.register._parseRegexes
const { expect } = require('chai')

const { RESULT, RESULT_FRAGMENT, REGEX, BAD_REGEX } = require('./constants')
describe('asciidoctor-regex-sub block tests', () => {
  beforeEach(() => {
    asciidoctor.Extensions.unregisterAll()
  })

  ;[
    {
      type: 'global',
      f: (text) => {
        asciidoctorregexSub.register(asciidoctor.Extensions)
        return asciidoctor.load(text)
      },
    },
    {
      type: 'registry',
      f: (text) => {
        const registry = asciidoctorregexSub.register(asciidoctor.Extensions.create())
        return asciidoctor.load(text, { extension_registry: registry })
      },
    },
  ].forEach(({ type, f }) => {
    [
      // ['====', 'example'],
      // ['----', 'listing'],
      // ['....', 'literal'],
      // ['--', 'open'],
      ['++++', 'pass'],
      // ['____', 'quote'],
      // ['****', 'sidebar'],
    ].forEach(([delimiter, context]) => {
      [
        `
:regex-sub: ${REGEX}

[regexSub]
`,
        `
[regexSub, ${REGEX}]
`,
        `
:regex-sub: ${BAD_REGEX}

[regexSub, ${REGEX}]
`,
      ].forEach((call) => {
        it(`plain link, ${type}`, () => {
          const doc = f(`
${call}
${delimiter}
<a class="el" href="dir_8d0824bbeddf28068400ad1b089826bf.adoc">
${delimiter}
`)
          const html = doc.convert()
          expect(html).to.contain(RESULT)
        })

        it(`fragment link, ${type}`, () => {
          const doc = f(`
${call}
${delimiter}
<a class="el" href="dir_8d0824bbeddf28068400ad1b089826bf.adoc#fragment.adoc">
${delimiter}
`)
          const html = doc.convert()
          expect(html).to.contain(RESULT_FRAGMENT)
        })
      })
    })
  })
})

describe('regex parsing tests', () => {
  it('null', () => {
    const actual = parseRegexes(null)
    const expected = {}
    expect(actual).to.deep.equal(expected)
  })

  it('simple', () => {
    const actual = parseRegexes('{ "(href=\\"[^.#]*)\\\\.adoc((#([^\\"]*))?\\")": "$1.html$2" }')
    const expected = {
      '(href="[^.#]*)\\.adoc((#([^"]*))?")': '$1.html$2',
    }
    expect(actual).to.deep.equal(expected)
  })

  it('two', () => {
    const actual = parseRegexes('{ "(href=\\"[^.#]*)\\\\.adoc((#([^\\"]*))?\\")": "$1.html$2",  "(href=\\"[^.#]*)\\\\.asciidoc((#([^\\"]*))?\\")": "$1.html$2"}')
    const expected = {
      '(href="[^.#]*)\\.adoc((#([^"]*))?")': '$1.html$2',
      '(href="[^.#]*)\\.asciidoc((#([^"]*))?")': '$1.html$2',
    }
    expect(actual).to.deep.equal(expected)
  })

  it('override', () => {
    const actual = parseRegexes('{ "(href=\\"[^.#]*)\\\\.adoc((#([^\\"]*))?\\")": "$1.xhtml$2",  "(href=\\"[^.#]*)\\\\.asciidoc((#([^\\"]*))?\\")": "$1.html$2"}',
      {
        '(href="[^.#]*)\\.adoc((#([^"]*))?")': '$1.html$2',
      })
    const expected = {
      '(href="[^.#]*)\\.adoc((#([^"]*))?")': '$1.xhtml$2',
      '(href="[^.#]*)\\.asciidoc((#([^"]*))?")': '$1.html$2',
    }
    expect(actual).to.deep.equal(expected)
  })
})
