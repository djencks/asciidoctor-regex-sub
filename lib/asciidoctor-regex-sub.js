'use strict'

const Opal = global.Opal

module.exports.register = function (registry, config = {}) {
  // For a per-page extension in Antora, config will have the structure:
  //{ file, // the vfs file being processed
  // contentCatalog, // the Antora content catalog
  // config // the asciidoc section of the playbook, enhanced with asciidoc attributes from the component descriptor.
  // }

  // For "required" global extensions in Antora, config will be undefined (replaced with {}).
  // Other loading code may use other config options.

  //File access

  const vfs = (config.vfs && typeof config.vfs.read === 'function')
    ? config.vfs
    //Antora support
    : (config.file && config.contentCatalog && typeof config.contentCatalog.resolveResource === 'function')
      ? {
        read: (resourceId) => {
          const target = config.contentCatalog.resolveResource(resourceId, config.file.src)
          return target.contents
        },
      }
      //look in the file system
      : fsAccess()

  function fsAccess () {
    const fs = require('fs')
    return {
      read: (path, encoding = 'utf8') => {
        if (path.startsWith('file://')) {
          return fs.readFileSync(path.substr('file://'.length), encoding)
        }
        return fs.readFileSync(path, encoding)
      },
    }
  }

  function substitutedBlock (parent, attributes, text, method, context) {
    const global = parseRegexes(parent.document.getAttribute('regex-sub'))
    const regex = parseRegexes(attributes.regex, global)
    delete attributes.regex
    delete attributes.$positional
    text = Object.entries(regex).reduce((accum, [re, sub]) => {
      return accum.replace(new RegExp(re, 'g'), sub)
    }, text)
    return method(parent, context, text, toHash(attributes))
  }

  function asciidoctorRegexSubBlock () {
    const self = this
    self.named('regexSub')
    self.onContext(['example', 'listing', 'literal', 'open', 'pass', 'quote', 'sidebar'])
    self.positionalAttributes(['regex'])
    self.process(function (parent, reader, attributes) {
      const text = reader.$read_lines().join('\n')
      const context = attributes['cloaked-context']
      return substitutedBlock(parent, attributes, text, self.$create_block, context)
    })
  }

  function asciidoctorRegexSubBlockMacro () {
    const self = this
    self.named('regexSub')
    self.positionalAttributes(['context', 'regex'])
    self.process(function (parent, target, attributes) {
      const text = vfs.read(target).toString()
      const context = attributes.context
      const result = substitutedBlock(parent, attributes, text, self.$create_block, context)
      if (result.content_model === 'compound') {
        self.$parse_content(result, result.lines, toHash(attributes))
      }
      return result
    })
  }

  function asciidoctorRegexSubInlineMacro () {
    const self = this
    self.named('regexSub')
    //I don't know how to get specifying the regex inline to work,
    //it is mangled by quotes substitution.
    self.positionalAttributes(['context', 'opts', 'regex'])
    self.process(function (parent, target, attributes) {
      const text = vfs.read(target).toString()
      const context = attributes.context
      const opts = attributes.opts ? JSON.parse(attributes.opts) : {}
      return substitutedBlock(parent, opts, text, self.$create_inline, context)
    })
  }

  function doRegister (registry) {
    if (typeof registry.block === 'function') {
      registry.block(asciidoctorRegexSubBlock)
    } else {
      console.warn('no \'block\' method on alleged registry')
    }
    if (typeof registry.blockMacro === 'function') {
      registry.blockMacro(asciidoctorRegexSubBlockMacro)
    } else {
      console.warn('no \'blockMacro\' method on alleged registry')
    }
    if (typeof registry.inlineMacro === 'function') {
      registry.inlineMacro(asciidoctorRegexSubInlineMacro)
    } else {
      console.warn('no \'inlineMacro\' method on alleged registry')
    }
  }

  if (typeof registry.register === 'function') {
    registry.register(function () {
      //Capture the global registry so processors can register more extensions.
      registry = this
      doRegister(registry)
    })
  } else {
    doRegister(registry)
  }
  return registry
}

var toHash = function (object) {
  return object && !object.$$is_hash ? Opal.hash2(Object.keys(object), object) : object
}

function parseRegexes (input, existing = {}) {
  const newRegex = input ? JSON.parse(input) : {}
  return Object.assign({}, existing, newRegex)
}

module.exports.register._parseRegexes = parseRegexes
